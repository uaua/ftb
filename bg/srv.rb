require 'rubygems'
require 'sinatra'
require 'sinatra/cross_origin'
require 'json'
require 'futaba'

register Sinatra::CrossOrigin

configure do
  enable :cross_origin
end

before do
  if request.request_method == 'OPTIONS'
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Methods"] = "POST"

    halt 200
  end
end

JSON.load_default_options[:symbolize_names] = true

post '/tintin' do
  content_type :json
  q = JSON.load(request.body)
  url = q[:url]
  board = Futaba::Board.new(url)
  # board.catalog.set_order :increasing
  board.catalog.set_letters 15
  board.catalog.threads.map do |e|
    {
      id: e.id,
      uri: e.uri,
      head_letters: e.head_letters,
      thumbnail: if e.thumbnail
      {
        uri: e.thumbnail.uri,
        height: e.thumbnail.height,
        width: e.thumbnail.width
      } else
      {
        uri: nil,
        height: nil,
        width: nil
      } end,
     n_posts: e.n_posts
    }
  end.to_json
end

post '/tintin/th' do
  content_type :json
  q = JSON.load(request.body)
  url = q[:url]
  thread = Futaba::Thread.new
  thread.uri = url
  thread.posts.map do |e|
    {
      no: e.no,
      title: e.title,
      name: e.name,
      id: e.id,
      ip: e.ip,
      mailto: e.mailto,
      date: e.date,
      body: e.body,
      image: if e.image
      {
        uri: e.image.uri,
        size_byte: e.image.size_byte,
        thumbnail: {
          uri: e.image.thumbnail.uri,
          height: e.image.thumbnail.height,
          width: e.image.thumbnail.width
        }
      } else
      {
        uri: nil,
        size_byte: 0,
        thumbnail: nil
      } end,
      deleted_p: e.deleted_p,
      soudane: e.soudane
    }
  end.to_json
end
