import { Component, OnInit } from '@angular/core';

import { Board } from './board';
import { Thread } from './thread'
import { Post } from './post'
import { CatalogService } from './catalog.service';
import { ThreadService } from './thread.service';

import 'rxjs/add/operator/switchMap';

@Component({
  moduleId: module.id,
  selector: 'thread',
  templateUrl: `thread.component.html`,
  providers: [ThreadService]
})

export class ThreadComponent implements OnInit {
  threads: Thread[] = [];
  posts: Post[];
  thread: Thread;

  constructor(
    private catalogService: CatalogService,
    private threadService: ThreadService) {
  }

  update(): void {
    console.log("update");
    this.threadService
        .getPosts(this.thread.uri)
        .then(posts => this.threadOnReceived(posts));
  }

  threadOnSelected(thread: Thread): void {
    this.thread = thread;
    this.update();
  }

  threadOnReceived(posts: Post[]): void {
    console.log(posts);
    this.posts = posts;
  }

  ngOnInit(): void {
    this.catalogService.selectedThread.subscribe(thread => this.threadOnSelected(thread));
  }
}
