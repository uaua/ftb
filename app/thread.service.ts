import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Post } from './post';

@Injectable()
export class ThreadService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private catalogUrl = 'http://localhost:4567/tintin/th';  // URL to web api

  constructor(private http: Http) { }

  getPosts(url: String): Promise<Post[]> {
    return this.http
      .post(this.catalogUrl, JSON.stringify({url: url}), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Post[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}