export class Thumbnail {
  uri: String;
  height: number;
  width: number;
}

export class Thread {
  id: number;
  uri: String;
  head_letters: String;
  thumbnail: Thumbnail;
  n_posts: number;
}