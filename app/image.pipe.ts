import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'imageFilter'})
export class ImageFilterPipe implements PipeTransform {
  transform(url: string): string {
    return url.split('/').pop();
  }
}