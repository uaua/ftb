import { Injectable } from '@angular/core';

import { Board } from './board';
import { BOARDS } from './mock-boards';

@Injectable()
export class BoardService {
  getBoards(): Promise<Board[]> {
    return Promise.resolve(BOARDS);
  }

  getBoard(i: number): Promise<Board> {
    return this.getBoards()
      .then(boards => boards[i]);
  }
}