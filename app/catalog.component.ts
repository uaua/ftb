import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { Board } from './board';
import { Thread } from './thread'
import { BoardService } from './board.service';
import { CatalogService } from './catalog.service';

import 'rxjs/add/operator/switchMap';

@Component({
  moduleId: module.id,
  selector: 'catalog',
  templateUrl: `catalog.component.html`,
  providers: [BoardService]
})

export class CatalogComponent implements OnInit {
  @Input()
  board: Board;
  threads: Thread[];

  constructor(
    private catalogService: CatalogService,
    private boardService: BoardService,
    private route: ActivatedRoute,
    private location: Location) { }

  getThreads(): void {
    this.catalogService
        .getThreads(this.board.url)
        .then(threads => this.threads = threads);
  }

  boardOnUpdate(board: Board): void {
    this.board = board;
    this.getThreads();
  }

  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.boardService.getBoard(+params['id']))
      .subscribe(board => this.boardOnUpdate(board));
  }
}
