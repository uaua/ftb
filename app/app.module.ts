import { NgModule, enableProdMode } from '@angular/core';
import { MaterialModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CatalogComponent } from './catalog.component';
import { ThreadComponent } from './thread.component';
import { CatalogService } from './catalog.service';

import { ImageFilterPipe } from './image.pipe';

const appRoutes: Routes = [
  {
    path: 'catalog/:id',
    component: CatalogComponent
  },
];

enableProdMode();

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    MaterialModule.forRoot(),
  ],
  declarations: [
    AppComponent,
    CatalogComponent,
    ThreadComponent,
    ImageFilterPipe
  ],
  providers: [
    CatalogService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
