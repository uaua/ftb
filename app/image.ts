export class Thumbnail {
  uri: String;
  height: number;
  width: number;
}

export class Image {
  uri: String;
  size_byte: number;
  thumbnail: Thumbnail;
}