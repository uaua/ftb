import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import {Subject} from 'rxjs/Subject';

import { Thread, Thumbnail } from './thread';

@Injectable()
export class CatalogService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private catalogUrl = 'http://localhost:4567/tintin';  // URL to web api
  selectedThread: Subject<Thread> = new Subject();

  constructor(private http: Http) { }

  getThreads(url: String): Promise<Thread[]> {
    return this.http
      .post(this.catalogUrl, JSON.stringify({url: url}), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Thread[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  selected(thread: Thread): void {
    this.selectedThread.next(thread);
  }
}