import { Component, OnInit } from '@angular/core';
import { Board } from './board';
import { Thread } from './thread';

import { BoardService } from './board.service';

@Component({
  moduleId: module.id,
  selector: 'ftb-app',
  templateUrl: `app.component.html`,
  providers: [BoardService]
})

export class AppComponent implements OnInit {
  boards: Board[];

  constructor(private boardService: BoardService) { }

  getBoards(): void {
    this.boardService.getBoards().then(boards => this.boards = boards);
  }

  ngOnInit(): void {
    this.getBoards();
  }
}
