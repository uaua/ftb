import { Image } from './image';

export class Post {
  no: number;
  title: String;
  name: String;
  id: String;
  ip: String;
  mailto: String;
  date: String;
  body: String;
  image: Image;
  delete_p: boolean;
  soudane: String;
}